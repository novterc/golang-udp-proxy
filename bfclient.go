package main

import(
    "fmt"
    "net"
    "time"
    "strconv"
)

const (
    CONN_HOST = "127.0.0.1"
    CONN_PORT_SERV = ":8002"
    CONN_TYPE = "udp"
)

func CheckError(err error) {
    if err  != nil {
        fmt.Println("Error: " , err)
    }
}
 
func main() {
    ServerAddr,err := net.ResolveUDPAddr(CONN_TYPE, CONN_HOST+CONN_PORT_SERV)
    CheckError(err)
 
    LocalAddr, err := net.ResolveUDPAddr(CONN_TYPE, "127.0.0.1:0")
    CheckError(err)
 
    Conn, err := net.DialUDP(CONN_TYPE, LocalAddr, ServerAddr)
    CheckError(err)
 
    defer Conn.Close()
    i := 0
    for {
        msg := strconv.Itoa(i)
        i++
        buf := []byte(msg)
        _,err := Conn.Write(buf)
        if err != nil {
            fmt.Println(msg, err)
        } else {
            fmt.Println(msg)
        }

        time.Sleep(time.Second * 1)
    }
}
