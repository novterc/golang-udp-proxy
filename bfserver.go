package main

import(
    "fmt"
    "net"
    "os"
    "encoding/json"
)

const (
    CONN_TYPE = "udp"
    CONFIGS_FILE_NAME = "serv_configs"
)

var (
    appConfigs Configuration
)

type Configuration struct {
    Host string `json:"host"`
    Port string `json:"port"`
}

func main() {
	fmt.Println("Run test server")

	loadConfigs()

    ServerAddr,err := net.ResolveUDPAddr(CONN_TYPE, appConfigs.Host + ":" + appConfigs.Port)
    checkError(err, "main-ServerAddr")

    ServerConn, err := net.ListenUDP(CONN_TYPE, ServerAddr)
    if checkError(err, "main-ListenUDP") {
	    for {
		    buf := make([]byte, 524)

	        n,addr,err := ServerConn.ReadFromUDP(buf)
	        fmt.Println("Received ",string(buf[0:n]), " from ",addr)

	        if checkError(err, "main-ReadFromUDP") {
	        	ServerConn.WriteToUDP([]byte("ack-"+string(buf[0:n])), addr)
	        } 
	    }
	}
}

func checkError(err error, name string) bool {
    if err  != nil {
        fmt.Println("Error " + name + ": " , err)
        return false
    }
    return true
}

func loadConfigs() {
    var config Configuration
    configFile, err := os.Open(CONFIGS_FILE_NAME)
    defer configFile.Close()
    checkError(err, "loadConfigs-configFile")
    jsonParser := json.NewDecoder(configFile)
    jsonParser.Decode(&config)
    appConfigs = config
}
