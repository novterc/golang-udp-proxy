package main

import(
    "fmt"
    "net"
    "os"
    "encoding/json"
    "encoding/hex"
    "runtime"
    // "time"
)

const (
    CONN_TYPE = "udp"
    CONFIGS_FILE_NAME = "configs"
    BUF_SIZE = 64000
)

var (
    // массив в котором хранятся коннекты 
    listConnectsServer [] ConnectItem
    proxyListnerConnect *net.UDPConn
    appConfigs Configuration
)

type Configuration struct {
    Host string `json:"host"`
    Port string `json:"port"`
    HostServ string `json:"host-serv"`
    PortServ string `json:"port-serv"`
    Debug int `json:"debug"`
}

// структура тип хранения коннектов содержит сам коннект с сервером и аддрес от куда пришел клиент чтобы при повторном пакете отправить на тодже коннект
type ConnectItem struct {
    udpconn *net.UDPConn
    addr string
}
 
func main() {
    fmt.Println("Run bf shell proxy. ")

    numcpu := runtime.NumCPU()
    fmt.Println("NumCPU", numcpu)
    runtime.GOMAXPROCS(numcpu)

    loadConfigs()

    // создаем слушающий порт прокси
    proxyListnerAddr,err := net.ResolveUDPAddr(CONN_TYPE, appConfigs.Host + ":" + appConfigs.Port)
    checkError(err, "main-proxyListnerAddr")

    proxyListnerConnect, err = net.ListenUDP(CONN_TYPE, proxyListnerAddr)
    if checkError(err, "main-proxyListnerConnect") {
    // defer proxyListnerConnect.Close()
        go proxyListner();

        var input string
        fmt.Scanln(&input)
    }
}

func loadConfigs() {
    var config Configuration
    configFile, err := os.Open(CONFIGS_FILE_NAME)
    defer configFile.Close()
    checkError(err, "loadConfigs-configFile")
    jsonParser := json.NewDecoder(configFile)
    jsonParser.Decode(&config)
    appConfigs = config

    fmt.Println(appConfigs)
}

func proxyListner() {
    // ожидаем данные от клиентов
    for {
        buf := make([]byte, BUF_SIZE)

        n,clientAddr,err := proxyListnerConnect.ReadFromUDP(buf)
        //нам что то прислали
        if checkError(err, "proxyListner-clientAddr") {

            if( appConfigs.Debug >= 7) {
                fmt.Println("read P <- C",hex.EncodeToString(buf[0:n]), " - ", clientAddr )
            }

            go proxyListnerHandler(clientAddr, buf, n)
        }
    }
}

func proxyListnerHandler(clientAddr *net.UDPAddr, buf []byte, n int) {
    // поиск существующего коннекта  .. кстате надо добавить таймаут и очищать массив иначе со временем
    findedServerConnect := findConnByAddr(clientAddr);
    // fmt.Println("findedConnProx: ",findedServerConnect)

    var err error

    var serverConnect *net.UDPConn
    if (findedServerConnect == nil) {
        // коннект с сервером не найден, будем создавать
        serverConnect,err = createServerConnect(clientAddr)
    } else {
        serverConnect = findedServerConnect
    }
    // defer serverConnect.Close()
    if checkError(err, "proxyListner-createServerConnect") {
        
        _,errProx := serverConnect.Write(buf)
        checkError(errProx, "proxyListner-serverConnect")

        if( appConfigs.Debug >= 7) {
            fmt.Println("send P -> S",hex.EncodeToString(buf[0:n]), " - ", clientAddr )
        }
    }    
}

func createServerConnect(clientAddr *net.UDPAddr) (*net.UDPConn, error) {
    if( appConfigs.Debug >= 1) {
        fmt.Println("create connect server for ", clientAddr)
    }
    ServerAddrProx,errProx := net.ResolveUDPAddr(CONN_TYPE, appConfigs.HostServ + ":" + appConfigs.PortServ)
    if checkError(errProx, "createServerConnect-ServerAddrProx") {
        
        // LocalAddrProx, errProx := net.ResolveUDPAddr(CONN_TYPE, appConfigs.Host + ":0")
        // if(checkError(errProx, "createServerConnect-LocalAddrProx")) {
         
            serverConnect, errProx := net.DialUDP(CONN_TYPE, nil, ServerAddrProx)
            if checkError(errProx, "createServerConnect-serverConnect") {
                if( appConfigs.Debug >= 1) {
                    fmt.Println("connect to server created ",  ServerAddrProx )
                }

                //создали запишем в массив
                listConnectsServer = append(listConnectsServer, ConnectItem{udpconn: serverConnect, addr: clientAddr.String()})
               
                go serverListner(serverConnect, clientAddr)

                return serverConnect, errProx
            } else {
                return nil, errProx
            }
        // } else {
        //     return nil, errProx   
        // }
    } else {
        return nil, errProx   
    }
}

func serverListner(serverConnect *net.UDPConn, clientAddr *net.UDPAddr) {
     if( appConfigs.Debug >= 8) {
        fmt.Println("begin working server listner")
    }
    for {
        buf := make([]byte, BUF_SIZE)

        n,addr,err := serverConnect.ReadFromUDP(buf)
        checkError(err, "serverListner-serverConnect")

        if( appConfigs.Debug >= 7) {
            fmt.Println("read P <- S :",hex.EncodeToString(buf[0:n]), " - ", addr, clientAddr )
        }

        proxyListnerConnect.WriteToUDP(buf, clientAddr)

        if( appConfigs.Debug >= 7) {
            fmt.Println("send P -> C :",hex.EncodeToString(buf[0:n]), " - ", addr, clientAddr )
        }
    }
}

// просто для вывода ошибок
func checkError(err error, name string) bool {
    if err  != nil {
        fmt.Println("Error " + name + ": " , err)
        return false
    }
    return true
}

// ищем коннект с сервером по адресу отправителя 
func findConnByAddr(findAddr net.Addr) *net.UDPConn {
    for _, item := range listConnectsServer {
        if item.addr == findAddr.String() {
            return item.udpconn
        }
    }

    return nil
}
